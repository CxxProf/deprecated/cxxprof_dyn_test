
#pragma once

#include "cxxprof_dyn_test/common.h"
#include <cxxprof_preloader/IDynCxxProf.h>

namespace CxxProf
{

    class CxxProf_Dyn_Test_EXPORT TestCxxProf : public IDynCxxProf
    {
    public:
        TestCxxProf();
        virtual ~TestCxxProf();

        std::unique_ptr<IActivity> createActivity(const std::string& name);
        void addMark(const std::string& name);
        void addPlotValue(const std::string& name, double value);
        void setProcessAlias(const std::string& name);
        void setThreadAlias(const std::string& name);
        void shutdown();

        std::string toString() const;
    };

}
